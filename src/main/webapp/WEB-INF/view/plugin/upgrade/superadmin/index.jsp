<%@page import="com.xnx3.wangmarket.plugin.upgrade.Global"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.xnx3.com/java_xnx3/xnx3_tld" prefix="x" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<jsp:include page="/wm/common/head.jsp">
	<jsp:param name="title" value="版本升级"/>
</jsp:include>
<script src="${STATIC_RESOURCE_PATH}js/fun.js"></script>


<div style="padding-top: 15%;font-size: 18px; padding-left: 40%;">
	<div>
		当前版本:v<%=Global.currentVersion() %>
	</div>
	<div id="versionTishi" style="padding-top: 20px;">
		(最新版本检测中...)
	</div>
	
</div>

<script>
//检测最新版本
function checkVersion(){
	wm.post("getNextVersion.json",{},function(result){
		if(result.result == '1'){
			document.getElementById('versionTishi').innerHTML = '<span style="color:red;">有新版本 '+result.info+' &nbsp; <button onclick="upgradeTishi(\''+result.info+'\');" class="layui-btn layui-btn-sm">点此升级</button></span>';
		}else{
			msg.info(result.info);
			document.getElementById('versionTishi').innerHTML = '目前已是最新版本';
		}
	});
}
checkVersion();

//传入版本号，弹出升级前的提示。传入格式如 v5.6.16
function upgradeTishi(nextVersion){
	var html = '将要从 <b>v<%=Global.currentVersion() %></b>升级到<b>'+nextVersion+'</b>'+
			   '<br/>为了数据安全考虑，请为您当前服务器做快照或镜像，作为备份，避免升级时出现意外';
	msg.confirm({
	    text:html,
	    buttons:{
	        立即升级:function(){
	        	upgrade(nextVersion);
	        },
	        取消:function(){
	            
	        }
	    } 
	});
}


//传入版本号，进行升级。传入格式如 v5.6.16
function upgrade(nextVersion){
	msg.loading('进行中');
	wm.post("upgrade.json",{nextVersion:nextVersion},function(result){
		msg.close();
		if(result.result == '1'){
			msg.loading('升级中...<br/>可能会持续几分钟，请稍后...');
			
			//延迟10秒执行
			setTimeout(function (){
				console.log('延迟20秒后判断是否启动起来');
				
				//每2秒判断一次tomcat是否启动起来了
				setInterval(function(){
					wm.post("getNextVersion.json", {}, function(result){
						msg.confirm('升级完毕，登录试试吧。',function(){
							parent.location.reload();
						});
					});
					console.log('每5秒判断一下是否启动起来');
				},5000);
			}, 20000);
			
		}else{
			msg.alert(result.info);
		}
	});
}
</script>

<jsp:include page="/wm/common/foot.jsp"></jsp:include> 