package com.xnx3.wangmarket.plugin.upgrade;

import com.xnx3.j2ee.util.SystemUtil;

/**
 * 全局信息
 * @author 管雷鸣
 *
 */
public class Global {
	/*
	 * 版本升级的shell文件所在目录,这个目录下，一是判断是否需要升级的文件，在一个是升级时执行的shell文件。 填写严格注意格式！
	 * 具体文件目录的说明，参考：  https://gitee.com/leimingyun/wangmarket_deploy/tree/master/upgrade/
	 */
	public static String upgradeShellFolder() {
		return SystemUtil.get("UPGRADE_PLUGIN_SHELL_FOLDER");
	}
	
	/**
	 * 获取当前系统的版本号
	 * @return 返回格式如 5.6.1
	 */
	public static String currentVersion() {
		return SystemUtil.get("UPGRADE_PLUGIN_VERSION");
	}
	
}
