package com.xnx3.wangmarket.plugin.upgrade.controller.superadmin;

import java.io.IOException;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import cn.zvo.http.Http;
import cn.zvo.http.Response;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.xnx3.FileUtil;
import com.xnx3.j2ee.pluginManage.controller.BasePluginController;
import com.xnx3.j2ee.service.SqlService;
import com.xnx3.j2ee.util.ActionLogUtil;
import com.xnx3.j2ee.util.ApplicationPropertiesUtil;
import com.xnx3.j2ee.util.ConsoleUtil;
import com.xnx3.j2ee.util.SystemUtil;
import com.xnx3.j2ee.vo.BaseVO;
import com.xnx3.wangmarket.plugin.upgrade.Global;
import net.sf.json.JSONArray;

/**
 * 网站管理后台
 * @author 管雷鸣
 */
@Controller(value="superadminUpgradePluginController")
@RequestMapping("/plugin/upgrade/superadmin/")
public class IndexController extends BasePluginController {
	@Resource
	private SqlService sqlService;

	/**
	 * 首页
	 */
	@RequestMapping("index${url.suffix}")
	public String list(HttpServletRequest request,Model model,
			@RequestParam(value = "path", required = false , defaultValue="") String path){
		if(!haveSuperAdminAuth()){
			return error(model, "请先登录");
		}
//		
		
		return "plugin/upgrade/superadmin/index";
	}
	
	/**
	 * 检查当前系统是否有下一个版本，判断的依据是 https://gitee.com/leimingyun/wangmarket_deploy/tree/master/upgrade
	 * @return 是否有可以在线更新的下一个版本
	 *  		<ul>
	 *  			<li>若有最新版本，result == BaseVO.SUCCESS, info返回版本号，如 v5.6.12 </li>
	 *  			<li>若无新版本，result = BaseVO.failure</li>
	 *  		</ul>
	 */
	@RequestMapping(value="getNextVersion.json", method= {RequestMethod.POST})
	@ResponseBody
	public BaseVO getNewVersion(HttpServletRequest request){
		if(!haveSuperAdminAuth()){
			return error("请先登录");
		}
			
		ActionLogUtil.insert(request, "检查当前系统是否有下一个升级版本");
		
		Http http = new Http();
//		HttpsUtil https = new HttpsUtil();
		Response res;
		try {
			res = http.get(Global.upgradeShellFolder()+"version.list");
		} catch (IOException e) {
			e.printStackTrace();
			return error("获取下一个版本异常："+e.getMessage());
		}
//		HttpResponse hr = https.get("https://gitee.com/leimingyun/wangmarket_deploy/raw/master/upgrade/version.list");
		if(res.getCode() != 200) {
			return error("获取下一个版本异常,http响应码："+res.getCode());
		}
		
		String text = res.getContent();
		String[] versions = text.split("\r|\n|,|，");
		ConsoleUtil.debug(JSONArray.fromObject(versions).toString());
		//G.VERSION = "5.6.16"; //模拟测试用
		String upgradeNextVersion = "";	//可以升级的下一个版本，格式: v5.6.17
		for (int i = 0; i<versions.length; i++) {
			String vLine = versions[i];
			if(vLine == null || vLine.trim().length() < 2 || vLine.indexOf("-") == -1) {
				continue;
			}
			
			//将每行的 v5.6.16-v5.6.17 分割
			String[] oldNewVersions = vLine.split("-");
			String oldVersion = oldNewVersions[0].trim();	//老版本
			String newVersion = oldNewVersions[1].trim();	//要升级到的新版本
			ConsoleUtil.debug(JSONArray.fromObject(oldNewVersions).toString());
			ConsoleUtil.debug("-- "+("v"+Global.currentVersion()));
			if(("v"+Global.currentVersion()).equalsIgnoreCase(oldVersion)) {
				ConsoleUtil.info("当前版本为:"+oldVersion+", 发现要升级的版本为："+newVersion);
				upgradeNextVersion = newVersion;
				System.out.println("upgradeNextVersion:"+upgradeNextVersion);
				return success(newVersion);
			}
		}
		
		return error("您的版本已是最新，当前无可升级的下一个版本");
	}
	
	/**
	 * 传入一个要升级的版本号，进行升级版本。必须先经过 getNewVersion 取得下一个升级版本才行，不能跳跃升级
	 * @param nextVersion 要升级到的下一个版本，传入格式如 v5.6.16
	 * @return
	 * @throws IOException 
	 */
	@RequestMapping(value="upgrade.json", method= {RequestMethod.POST})
	@ResponseBody
	public BaseVO upgrade(HttpServletRequest request,
			@RequestParam(value = "nextVersion", required = false, defaultValue="") String nextVersion) throws IOException{
		if(!haveSuperAdminAuth()){
			return error("请先登录");
		}
		if(nextVersion.trim().length() < 1 || nextVersion.indexOf("v") < 0) {
			return error("newVersion参数异常，传入格式如： v5.6.16");
		}
		
		//shell更新文件
		String shellUrl = Global.upgradeShellFolder()+"v"+Global.currentVersion()+"-"+nextVersion+".sh";
		System.out.println(shellUrl);
		//判断shell文件是否存在
//		HttpsUtil https = new HttpsUtil();
		Http http = new Http();
		Response res = http.get(shellUrl);
//		HttpResponse hr = https.get(shellUrl);
		if(res.getCode() != 200) {
			return error("升级的shell文件不存在："+shellUrl);
		}
		
		/**** 进行一些校验 ****/
		//判断数据库是否是mysql数据库
		String driver = ApplicationPropertiesUtil.getProperty("spring.datasource.driver-class-name");
		if(driver.indexOf("mysql") < 1) {
			return error("您当前使用的不是mysql数据库，不支持在线自动升级。");
		}
		//判断是否是linux系统
		if(SystemUtil.isWindowsOS()) {
			return error("您当前使用的是windows系统，只有完全按照我们安装文档来的 CentOS 7.4、7.6 的系统才支持在线自动升级。");
		}
		//判断是否是jar包方式运行的开发环境
		if(com.xnx3.j2ee.Global.isJarRun) {
			return error("您当前是在开发环境中运行的，本升级是必须在线上正式环境才能使用。");
		}
		//判断安装路径是否正确，是否安装到了 /mnt/tomcat8/webapps/ROOT/ 路径下
		if(!FileUtil.exists("/mnt/tomcat8/webapps/ROOT/WEB-INF/classes/application.properties")) {
			return error("自动检测发现 /mnt/tomcat8/webapps/ROOT/WEB-INF/classes/application.properties 不存在！并非根据我们安装文档来安装的，不支持在线升级。");
		}
		
		
		/**** 执行shell命令，wget下载shell文件并执行 ****/
		/*
		
		 mkdir /mnt/tomcat8/upgrade/ && cd /mnt/tomcat8/upgrade/ && 。。。下面的，到时候自己拼接
		 
		 wget 上面的shellUrl -O v"+G.VERSION+"-"+newVersion+".sh"   //从上面复制来的，大概是这个意思
		 chmod -r 777 xxxxxx.sh
		 chmod -x xxxxx.sh
		 ./xxxx.sh> xxxx	//关闭tomcat、更新文件、重启服务器操作 也在这个shell文件中，所以只要执行了这个shell文件，那么就已经完成了升级。另外这个shell文件执行的日志要保存出来，比如这个shell文件名字为 v5.6-v5.7.sh ,那执行的日志文件要保存为 v5.6-v5.7.log
		 
		 */
		String and = " && ";
		StringBuilder shell = new StringBuilder("");
		// 创建目录，存放升级文件
		shell.append("mkdir -p /mnt/tomcat8/upgrade/");
		// 前往创建的目录
		shell.append(and + "cd /mnt/tomcat8/upgrade/");
		// 通过wget下载升级文件
		shell.append(and + "wget " + shellUrl);
		// 为文件设置权限
		String updateFileName = "v" + Global.currentVersion() + "-" + nextVersion + ".sh";
		shell.append(and + "chmod -R 777 ./" + updateFileName);
		// 替换字符
		shell.append(and + "sed 's/\\r//' -i " + updateFileName);
		// 升级并输出日志文件
		String logFileName = "v" + Global.currentVersion() + "-" + nextVersion + ".log";
		shell.append(and + "/bin/bash ./" + updateFileName + " >> " + logFileName + " 2>&1");
		// 执行指令
		String [] cmd = {"/bin/sh", "-c", shell.toString()};
		Runtime.getRuntime().exec(cmd);

		return success();
	}
	
	public static void main(String[] args) throws IOException {
		
//		HttpsUtil https = new HttpsUtil();
		Http http = new Http();
		Response res = http.get("https://gitee.com/leimingyun/wangmarket_deploy/raw/master/upgrade/version.list");
		String text = res.getContent();
		System.out.println(text);
		String[] versions = text.split("\r|\n|,|，");
		System.out.println(JSONArray.fromObject(versions));
	}
}