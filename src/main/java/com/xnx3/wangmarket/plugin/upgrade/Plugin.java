package com.xnx3.wangmarket.plugin.upgrade;

import com.xnx3.j2ee.pluginManage.PluginRegister;

/**
 * 版本升级
 * @author 管雷鸣
 *
 */
@PluginRegister(menuTitle="版本升级", menuHref="/plugin/upgrade/superadmin/index.do", intro="wangmarket 通过总管理后台的功能插件-系统升级， 进行一键升级", applyToSuperAdmin = true , version="1.1", versionMin="6.0")
public class Plugin{

}
