
## 功能介绍
[wangmarket（网市场云建站系统、雷鸣云客服系统）](https://gitee.com/mail_osc/wangmarket) 的 版本升级 插件  
通过总管理后台的功能插件-系统升级， 进行一键升级更新到新版本。

## 在别的项目中使用方式
比如在客服系统中使用时，需要额外将 com.xnx3.wangmarket.plugin.upgrade.Global.java 也在主包创建，来覆盖jar包中的这个，并配置上客服系统的升级目录

## 使用条件
1. 本项目(生成的 target/wangmarket-plugin-xxx.jar)放到 [网市场云建站系统](https://gitee.com/leimingyun/wangmarket_deploy) 中才可运行使用
1. 网市场云建站系统本身需要 v5.7 或以上版本。
1. 本项目只支持 mysql 数据库。使用默认 sqlite 数据库的不可用
1. 本项目必须按照我们文档的安装步骤来的才能使用。如果你tomcat路径有改动将不能使用本插件。

## 使用方式
1. 下载 /target/ 中的jar包
1. 将下载的jar包放到 tomcat/webapps/ROOT/WEB-INF/lib/ 下
1. 重新启动运行项目，登陆总管理后台的功能插件-系统升级，找到 版本升级


## 二次开发
#### 本插件的二次开发
1. 运行项目，使用admin账号（密码也是admin）登录总管理后台
1. 找到左侧的 功能插件-版本升级，进入即可看到

#### 从头开始开发一个自己的插件
参考文档：  https://gitee.com/leimingyun/dashboard/wikis/leimingyun/wm/preview?sort_id=3213258&doc_id=1101390

#### 二次开发wangmarket及功能的扩展定制
可参考：  https://gitee.com/leimingyun/wangmarket_deploy

## 合作洽谈
作者：管雷鸣<br/>
微信：xnx3com<br/>
QQ：921153866<br/>
